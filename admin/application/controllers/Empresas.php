<?php

class Empresas extends CI_Controller
{

	public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('logged') != true || $this->session->userdata('id_grupo') != 1){

        	$this->session->sess_destroy();
            redirect('login');

        }

    }

	public function index()
	{
		$this->load->model('Empresas_Model');

		$empresas = $this->Empresas_Model->findAll();

		$this->load->view('layout/topo');
		$this->load->view('empresas/index', array('empresas' => $empresas));
		$this->load->view('layout/rodape');
	}

	public function inserir()
	{	
		$this->load->model('Eventos_Model');

		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nome_empresa', 'Nome da empresa ', 'required|max_length[200]');
		$this->form_validation->set_rules('num_contrato', 'Nº de contrato ', 'required|max_length[100]');
		$this->form_validation->set_rules('cnpj_empresa', 'CNPJ ', 'required|max_length[100]');
		$this->form_validation->set_rules('nome_responsavel[]', 'Nome do responsável', 'required|max_length[200]');
		$this->form_validation->set_rules('email_responsavel[]', 'E-mail do responsável', 'required|max_length[200]');
		$this->form_validation->set_rules('id_evento', 'Evento', 'required');

		if($this->form_validation->run() == false) {
			$eventos = $this->Eventos_Model->findAll();

			$this->load->view('layout/topo');
			$this->load->view('empresas/inserir', array('eventos' => $eventos));
			$this->load->view('layout/rodape');

		} else {

			// carregando models necessários
			$this->load->model('Usuarios_Model');
			$this->load->model('Empresas_Model');
			
			####################################
			# CADASTRANDO O USUÁRIO DA EMPRESA #
			####################################
			
			// gerando o login da empresa
			$login = strtolower(strtr($this->input->post('nome_empresa')," ","_"));
			
			// gerando uma senha aleatória de 8 digitos
			$this->load->helper('string');
			$senha = random_string('alnum',8);

			// verifica se existe usuário com o mesmo login
			$verifica = $this->Usuarios_Model->verificaUsuario($login);

			if($verifica) {
				//se existir o usuario, o sistema cria outro
				$login = $login . '2';
			}

			$dadosUsuario = array(
				'login'			=> $login,
				'senha'			=> $senha,
				'nome_usuario'	=> $this->input->post('nome_empresa'),
				'id_grupo'		=> 2
			);
			
			$id_usuario = $this->Usuarios_Model->insert($dadosUsuario);

			#########################
			# CADASTRANDO A EMPRESA #
			#########################

			//recebendo os dados via post e colocando na variável dados
			$dadosEmpresa = array(
				'nome_empresa'		=> $this->input->post('nome_empresa'),
				'num_contrato'		=> $this->input->post('num_contrato'),
				'cnpj_empresa'		=> $this->input->post('cnpj_empresa'),
				'id_usuario'		=> $id_usuario,
				'id_evento'			=> $this->input->post('id_evento')
			);

			$id_empresa = $this->Empresas_Model->insert($dadosEmpresa);


			##########################################
			# CADASTRANDO OS RESPONSÁVEIS DA EMPRESA #
			##########################################
			$nomes = $this->input->post('nome_responsavel[]');
			$emails = $this->input->post('email_responsavel[]');
			$count = count($nomes) - 1;

			for ($i=0; $i <= $count ; $i++) { 
				
				$dadosResponsaveis = array(
					'id_empresa' => $id_empresa,
					'nome_responsavel' => $nomes[$i],
					'email_responsavel' => $emails[$i]
				);	

				$this->Empresas_Model->salvarResponsavel($dadosResponsaveis);

			}

			$this->session->set_flashdata(array(
				'msg'	=> 'Empresa cadastrada com sucesso!',
				'type'	=> 'success'
			));
			
			redirect('empresas');

		}
	}

	public function editar($id)
	{
		$this->load->model('Empresas_Model');
		$this->load->model('Eventos_Model');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nome_empresa', 'Nome da empresa ', 'required|max_length[200]');
		$this->form_validation->set_rules('num_contrato', 'Nº de contrato ', 'required|max_length[100]');
		$this->form_validation->set_rules('cnpj_empresa', 'CNPJ ', 'required|max_length[100]');
		$this->form_validation->set_rules('nome_responsavel[]', 'Nome do responsável ', 'required|max_length[200]');
		$this->form_validation->set_rules('email_responsavel[]', 'E-mail do responsável ', 'required|max_length[200]');


		if($this->form_validation->run() == false) {

			$empresa = $this->Empresas_Model->find($id);
			$responsaveis = $this->Empresas_Model->findResponsaveis($id);
			$eventos = $this->Eventos_Model->findAll();

			$this->load->view('layout/topo');
			$this->load->view('empresas/editar', array('empresa' => $empresa, 'responsaveis' => $responsaveis, 'eventos' => $eventos));
			$this->load->view('layout/rodape');

		} else {

			####################
			# SALVANDO EMPRESA #
			####################
			$dadosEmpresa = array(
				'nome_empresa'	=> $this->input->post('nome_empresa'),
				'num_contrato'	=> $this->input->post('num_contrato'),
				'cnpj_empresa'	=> $this->input->post('cnpj_empresa'),
				'id_evento'		=> $this->input->post('id_evento')
			);

			$this->Empresas_Model->update($dadosEmpresa, $id);

			#########################
			# SALVANDO RESPONSÁVEIS #
			#########################
			$nomes = $this->input->post('nome_responsavel[]');
			$emails = $this->input->post('email_responsavel[]');
			$id_responsavel = $this->input->post('id_responsavel[]');
			$count = count($nomes) - 1;

			for ($i=0; $i <= $count ; $i++) { 
				
				$dadosResponsaveis = array(
					'nome_responsavel'	=> $nomes[$i],
					'email_responsavel'	=> $emails[$i]
				);

				if(isset($id_responsavel[$i])) {

					$this->Empresas_Model->updateResponsavel($dadosResponsaveis, $id_responsavel[$i]);

				}else {
					$dadosResponsaveis['id_empresa'] = $id;

					$this->Empresas_Model->salvarResponsavel($dadosResponsaveis);

				}
			}

			$this->session->set_flashdata(array(
				'msg'	=> 'Empresa salva com sucesso!',
				'type'	=> 'success'
			));

			redirect('empresas');
		}
	}

	public function acessos()
	{
		$this->load->model('Empresas_Model');

		$empresas = $this->Empresas_Model->findAcessos();

		$data = date('Y-m-d H:i:s');

		$this->load->view('layout/topo');
		$this->load->view('empresas/acessos', array('empresas' => $empresas, 'data' => $data));
		$this->load->view('layout/rodape');

	}
}
