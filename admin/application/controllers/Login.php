<?php

class Login extends CI_Controller
{
	public function index()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('login', 'Login', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');

		if($this->form_validation->run() == false) {
			
			$this->load->view('login');


		} else {

			$this->load->model('Admin_Model');

			$usuario = $this->Admin_Model->get_login($this->input->post('login'), $this->input->post('senha'));

			// Se retornar true o usuario, será feito o login
			if($usuario) {
				
				$this->session->set_userdata(array(
                    'logged'		=> true,
                    'id_usuario'	=> $usuario->id_usuario,
                    'id_grupo'		=> $usuario->id_grupo,
                    'nome_usuario'	=> $usuario->nome_usuario
                ));

                // Salvar usuário na tabela de acessos
                $this->Admin_Model->logAcesso($usuario->id_usuario);

                if($usuario->id_grupo == 1){

                	redirect('welcome');
                
                } elseif ($usuario->id_grupo == 3) {
                	
                	redirect('financeiro');
                
                } elseif ($usuario->id_grupo == 2) {

                	redirect('welcome');
                }
                
                

            
			} else {// senão jogará a mensagem de erro na tela

                $this->session->set_flashdata(array(
                    'type'  => 'danger',
                    'msg'   => 'Login ou senha inválidos!'
                ));

                $this->load->view('login');

			}

		}
	}

	public function logout()
    {
        $this->session->unset_userdata('logged');
        $this->session->unset_userdata('id_usuario');
        $this->session->unset_userdata('id_grupo');
        $this->session->unset_userdata('login');
        
        redirect('login');
    }
}