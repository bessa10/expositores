<?php

class Eventos extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('logged') != true || $this->session->userdata('id_grupo') != 1){

        	$this->session->sess_destroy();
            redirect('login');

        }

    }
    
	public function index() 
	{
		$this->load->model('Eventos_Model');

		$eventos = $this->Eventos_Model->findAll();

		$this->load->view('layout/topo');
		$this->load->view('eventos/index', array('eventos' => $eventos));
		$this->load->view('layout/rodape');
	}

	public function inserir() 
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('num_centro_custo', 'Nº centro de custo ', 'required|max_length[50]');
		$this->form_validation->set_rules('nome_evento', 'Nome do evento ', 'required|max_length[200]');
		$this->form_validation->set_rules('data_inicio', 'Data de início ', 'required');
		$this->form_validation->set_rules('data_termino', 'Data do término ', 'required');
		$this->form_validation->set_rules('organizador', 'Organizador ', 'required');

		if($this->form_validation->run() == false) {

			$this->load->model('Empresas_Model');

			$empresas = $this->Empresas_Model->findAll();

			$this->load->view('layout/topo');
			$this->load->view('eventos/inserir', array('empresas' => $empresas));
			$this->load->view('layout/rodape');


		} else {

			$dados = array(
				'num_centro_custo'	=> $this->input->post('num_centro_custo'),
				'nome_evento'		=> $this->input->post('nome_evento'),
				'data_inicio'		=> $this->input->post('data_inicio'),
				'data_termino'		=> $this->input->post('data_termino'),
				'organizador'		=> $this->input->post('organizador')
			);

			$this->load->model('Eventos_Model');

			$this->Eventos_Model->insert($dados);			

			$this->session->set_flashdata(array(
				'msg'	=> 'Evento cadastrado com sucesso!',
				'type'	=> 'success'
			));
			
			redirect('eventos');
		}
	}

	public function editar($id)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('num_centro_custo', 'Nº centro de custo ', 'required|max_length[50]');
		$this->form_validation->set_rules('nome_evento', 'Nome do evento ', 'required|max_length[200]');
		$this->form_validation->set_rules('data_inicio', 'Data de início ', 'required');
		$this->form_validation->set_rules('data_termino', 'Data do término ', 'required');
		$this->form_validation->set_rules('organizador', 'Organizador ', 'required');

		$this->load->model('Eventos_Model');
		$this->load->model('Empresas_Model');

		if($this->form_validation->run() == false) {

			$empresas = $this->Empresas_Model->findAll();
			$evento = $this->Eventos_Model->find($id);

			$this->load->view('layout/topo');
			$this->load->view('eventos/editar', array('evento' => $evento, 'empresas' => $empresas));
			$this->load->view('layout/rodape');

		} else {

			$dadosUpdate = array(
				'num_centro_custo'	=> $this->input->post('num_centro_custo'),
				'nome_evento'		=> $this->input->post('nome_evento'),
				'data_inicio'		=> $this->input->post('data_inicio'),
				'data_termino'		=> $this->input->post('data_termino'),
				'organizador'		=> $this->input->post('organizador')
			);

			$this->Eventos_Model->update($dadosUpdate, $id);

			$this->session->set_flashdata(array(
				'msg'	=> 'Evento salvo com sucesso!',
				'type'	=> 'success'
			));

			redirect('eventos');

		}
	}

}