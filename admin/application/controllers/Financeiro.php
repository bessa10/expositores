<?php

class Financeiro extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('logged') != true || $this->session->userdata('id_grupo') == 2){

        	$this->session->sess_destroy();
            redirect('login');

        }
        
    }

	public function index()
	{
		$this->load->model('Empresas_Model');

		if($this->input->post()) {

			$dadosFiltro = array(
				'nome_empresa' => $this->input->post('nome_empresa'),
				'num_contrato' => $this->input->post('num_contrato'),
				'cnpj_empresa' => $this->input->post('cnpj_empresa')
			);

			$this->session->set_flashdata(array(
				'nome_empresa' => $dadosFiltro['nome_empresa'],
				'num_contrato' => $dadosFiltro['num_contrato'],
				'cnpj_empresa' => $dadosFiltro['cnpj_empresa']
			));

			$empresas = $this->Empresas_Model->findFiltro($dadosFiltro);

		} else {

			$empresas = $this->Empresas_Model->findAll();

		}

		$this->load->view('layout/topo');
		$this->load->view('financeiro/index', array('empresas' => $empresas));
		$this->load->view('layout/rodape');
	}

	public function confirmar_pagamento($id)
	{
		$this->load->model('Empresas_Model');

		$registro = $this->Empresas_Model->confirmar_pagamento($id);

		if($registro) {

			$this->session->set_flashdata(array(
				'msg'	=> 'Pagamento confirmado com sucesso!',
				'type'	=> 'success'
			));

			redirect('financeiro');

		} else {

			$this->session->set_flashdata(array(
				'msg'	=> 'Não foi possível confirmar o pagamento!',
				'type'	=> 'danger'
			));

			redirect('financeiro');
		}
	}

	public function cancelar_pagamento($id)
	{
		$this->load->model('Empresas_Model');

		$registro = $this->Empresas_Model->cancelar_pagamento($id);

		if($registro) {

			$this->session->set_flashdata(array(
				'msg'	=> 'Pagamento cancelado com sucesso!',
				'type'	=> 'success'
			));

			redirect('financeiro');

		} else {

			$this->session->set_flashdata(array(
				'msg'	=> 'Não foi possível cancelar o pagamento!',
				'type'	=> 'danger'
			));

			redirect('financeiro');
		}
	}
}