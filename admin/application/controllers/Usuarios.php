<?php

class Usuarios extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('logged') != true || $this->session->userdata('id_grupo') != 1){

        	$this->session->sess_destroy();
            redirect('login');

        }
    }
    
	public function index()
	{
		$this->load->model('Usuarios_Model');

		$usuarios = $this->Usuarios_Model->findAll();

		$this->load->view('layout/topo');
		$this->load->view('usuarios/index', array('usuarios' => $usuarios));
		$this->load->view('layout/rodape');
	}

	public function inserir()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nome_usuario', 'Nome ', 'required');
		$this->form_validation->set_rules('login', 'Login ', 'required|is_unique[tb_usuarios.login]');
		$this->form_validation->set_rules('senha', 'Senha ', 'required|min_length[6]|max_length[10]');
		$this->form_validation->set_rules('confirmar_senha', 'Confirmar senha', 'required|matches[senha]|min_length[6]|max_length[10]');
		$this->form_validation->set_rules('id_grupo', 'Grupo de usuário ', 'required');

		$this->load->model('Usuarios_Model');

		if($this->form_validation->run() == false) {

			$grupos = $this->Usuarios_Model->findGrupos();

			$this->load->view('layout/topo');
			$this->load->view('usuarios/inserir', array('grupos' => $grupos));
			$this->load->view('layout/rodape');


		} else {

			$dados = array(
				'login'			=> $this->input->post('login'),
				'senha'			=> $this->input->post('senha'),
				'nome_usuario'	=> $this->input->post('nome_usuario'),
				'id_grupo'		=> $this->input->post('id_grupo')
			);

			$this->Usuarios_Model->insert($dados);

			$this->session->set_flashdata(array(
				'msg'	=> 'Usuário inserido com sucesso!',
				'type'	=> 'success'
			));

			redirect('usuarios');

		}
		
	}

	public function editar($id)
	{
		$this->load->model('Usuarios_Model');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('nome_usuario', 'Nome do usuário ', 'required');
		$this->form_validation->set_rules('senha', 'Senha ', 'min_length[6]|max_length[10]');
		$this->form_validation->set_rules('confirmar_senha', 'Confirmar senha ', 'matches[senha]|min_length[6]|max_length[10]');
		$this->form_validation->set_rules('id_grupo', 'Grupo de usuário ', 'required');

		if($this->form_validation->run() == false) {

			$usuario = $this->Usuarios_Model->find($id);
			$grupos = $this->Usuarios_Model->findGrupos();

			if(!$usuario){

				$this->session->set_flashdata(array(
					'msg'	=> 'Não foi possível encontrar o usuário!',
					'type'	=> 'danger'
				));

				redirect('usuarios');
			}

			$this->load->view('layout/topo');
			$this->load->view('usuarios/editar', array('usuario' => $usuario, 'grupos' => $grupos));
			$this->load->view('layout/rodape');
		
		} else {

			$dados['nome_usuario'] = $this->input->post('nome_usuario');
			$dados['id_grupo'] = $this->input->post('id_grupo');
			$dados['usuario_ativo'] = $this->input->post('usuario_ativo');

			if($this->input->post('senha') != NULL) {
				$dados['senha'] = $this->input->post('senha');
			}

			$this->Usuarios_Model->update($id, $dados);

			$this->session->set_flashdata(array(
				'msg'	=> 'Usuário alterado com sucesso!',
				'type'	=> 'success'
			));

			redirect('usuarios');
		}
	}

	public function enviar_emails()
	{
		// carrega os models que irão utilizar
		$this->load->model('Empresas_Model');
		$this->load->model('Usuarios_Model');

		// faz um foreach nas empresas selecionadas para enviar email
		foreach ($this->input->post('email') as $id_responsavel) {
			// faz a busca dos dados necessários para enviar o e-mail
			$empresa = $this->Empresas_Model->findEmailDados($id_responsavel);

			// se a empresa não for encontrada, retorna para a tela de acessos e mostra a msg de erro na tela
			if(!$empresa) {

				$this->session->set_flashdata(array(
					'msg'	=> 'Não foi possível encontrar dados de e-mail da empresa!',
					'type'	=> 'danger'
				));

				redirect('empresas/acessos');
			}

			########################################################
			# INÍCIO DO CÓDIGO PARA ENVIAR E-MAIL PARA OS USUÁRIOS #
			########################################################

			//carrega a library
			$this->load->library('email');

			//Inicia o processo de configuração para o envio do email
	        $config['protocol'] = 'mail'; // define o protocolo utilizado
	        $config['mailtype'] = 'html'; // define o tipo de email como html

	        // Inicializa a library Email, passando os parâmetros de configuração
	        $this->email->initialize($config);

	        // Define remetente e destinatário
	        $this->email->from('noreply@abacos.com.br', $empresa->nome_evento); // Remetente
	        $email = $empresa->email_responsavel;
	        $this->email->to($email, $empresa->nome_responsavel); // Destinatário
	        $this->email->cc('bruno@abacos.com.br');

	        // Define o assunto do email
	        $this->email->subject('Seu acesso ao sistema já está dispnível');

	        //Passando os dados para o template de email
	        $body = $this->load->view('layout/email-usuario',array('empresa' => $empresa),TRUE);
	        
	        $this->email->message($body);

	        // Salvar horário de envio do email
        	$this->Usuarios_Model->LogEmail($empresa->id_usuario);

        	$autoriza = $this->Usuarios_Model->autorizaEmail($email);

        	if($autoriza) {
	        	// se o envio foi feito, mostre a mensagem de sucesso na tela
	       		$this->email->send();
	       	}

	    } // fim do foreach
	    
        $this->session->set_flashdata(array(
			'msg'	=> 'E-mail enviado com sucesso!',
			'type'	=> 'success'
		));

		redirect('empresas/acessos');

		if(!$this->email->send()) {

			$this->session->set_flashdata(array(
				'msg'	=> 'Não foi possível enviar o e-mail, tente novamente!',
				'type'	=> 'danger'
			));

            $this->load->view('empresas/acessos');

		}
	}
}