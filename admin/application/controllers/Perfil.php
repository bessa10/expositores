<?php 

class Perfil extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        
        if(!($this->session->userdata('logged') || $this->session->userdata('id_usuario'))) {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

	public function index()
	{
		$this->load->model('Usuarios_Model');

		$this->load->library('form_validation');

		$this->form_validation->set_rules('senha', 'Nova senha', 'min_length[6]|max_length[10]');
		$this->form_validation->set_rules('confirmar_senha', 'Confirmar nova senha', 'matches[senha]|min_length[6]|max_length[10]');

		if($this->form_validation->run() == false) {

			$usuario = $this->Usuarios_Model->find($this->session->userdata('id_usuario'));

			$this->load->view('layout/topo');
			$this->load->view('perfil-usuario', array('usuario' => $usuario));
			$this->load->view('layout/rodape');

		} else {

			if($this->input->post('senha') != NULL) {

				$id = $this->input->post('id_usuario');
				$dados['senha'] = $this->input->post('senha');

				$this->Usuarios_Model->update($id, $dados);
			}

			$this->session->set_flashdata(array(
				'msg'	=> 'Usuário alterado com sucesso!',
				'type'	=> 'success'
				));

			redirect('perfil');
		}
	}
}