<?php

class Eventos_Model extends CI_Model
{
	public function findAll()
	{
		$eventos = $this->db
					->select('tb_eventos.id_evento')
					->select('tb_eventos.num_centro_custo')
					->select('tb_eventos.nome_evento')
					->select("DATE_FORMAT(tb_eventos.data_inicio, '%d/%m/%Y') as data_inicio")
					->select("DATE_FORMAT(tb_eventos.data_termino, '%d/%m/%Y') as data_termino")
					->select('tb_eventos.organizador')
					->get('tb_eventos')
					->result();

		return $eventos;
	}

	public function find($id)
	{
		$evento = $this->db
					->where('id_evento', $id)
					->get('tb_eventos')
					->row();

		return $evento;
	}

	public function insert($dados = NULL)
	{
		if($dados != NULL) {

			$this->db->insert('tb_eventos', $dados);

			$id = $this->db->insert_id('tb_eventos');

			return $id;

		}
	}

	public function update($dadosUpdate = NULL, $id)
	{
		if($dadosUpdate != NULL) {

			$this->db->where('id_evento', $id)->update('tb_eventos', $dadosUpdate);

			return true;

		} else {

			return false;
		}

	}
}