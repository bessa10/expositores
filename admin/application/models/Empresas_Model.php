<?php

class Empresas_Model extends CI_Model
{
	public function findAll()
	{
		$empresas = $this->db
					->join('tb_eventos', 'tb_eventos.id_evento = tb_empresas.id_evento')
					->get('tb_empresas')
					->result();

		return $empresas;

	}

	public function findFiltro($dadosFiltro = NULL)
	{
		$empresas = $this->db
						->like('nome_empresa', $dadosFiltro['nome_empresa'])
						->like('num_contrato', $dadosFiltro['num_contrato'])
						->like('cnpj_empresa', $dadosFiltro['cnpj_empresa'])
						->get('tb_empresas')
						->result();

		return $empresas;
	}

	public function findAcessos()
	{
		$empresas = $this->db
					->select('tb_empresas.id_empresa')
					->select('tb_empresas.nome_empresa')
					->select('tb_responsaveis.id_responsavel')
					->select('tb_responsaveis.nome_responsavel')
					->select('tb_responsaveis.email_responsavel')
					->select('tb_usuarios.login')
					->select('tb_usuarios.senha')
					->select("DATE_FORMAT(tb_usuarios.ultimo_acesso, '%d/%m/%Y %H:%i') as ultimo_acesso")
					->select("DATE_FORMAT(tb_usuarios.ultimo_email, '%d/%m/%Y %H:%i') as ultimo_email")
					->join('tb_responsaveis', 'tb_responsaveis.id_empresa = tb_empresas.id_empresa')
					->join('tb_usuarios', 'tb_usuarios.id_usuario = tb_empresas.id_usuario')
					->where('pagamento', 'S')
					->get('tb_empresas')
					->result();

		return $empresas;
	}

	public function findEmailDados($id_responsavel)
	{
		$empresa = $this->db
					->select('tb_empresas.id_empresa')
					->select('tb_empresas.nome_empresa')
					->select('tb_usuarios.id_usuario')
					->select('tb_usuarios.login')
					->select('tb_usuarios.senha')
					->select('tb_responsaveis.id_responsavel')
					->select('tb_responsaveis.nome_responsavel')
					->select('tb_responsaveis.email_responsavel')
					->select('tb_eventos.nome_evento')
					->join('tb_responsaveis', 'tb_responsaveis.id_empresa = tb_empresas.id_empresa')
					->join('tb_usuarios', 'tb_usuarios.id_usuario = tb_empresas.id_usuario')
					->join('tb_eventos', 'tb_eventos.id_evento = tb_empresas.id_evento')
					->where('tb_responsaveis.id_responsavel', $id_responsavel)
					->get('tb_empresas')
					->row();

		return $empresa;
	}

	public function find($id)
	{
		$empresa = $this->db
					->join('tb_eventos', 'tb_eventos.id_evento = tb_empresas.id_evento')
					->where('tb_empresas.id_empresa', $id)
					->get('tb_empresas')
					->row();

		return $empresa;
	}

	public function findResponsaveis($id)
	{
		$responsaveis = $this->db
						->where('id_empresa', $id)
						->get('tb_responsaveis')
						->result();

		return $responsaveis;
	}

	public function insert($dados = NULL)
	{

		if($dados != NULL) {

			$this->db->insert('tb_empresas', $dados);

			$id = $this->db->insert_id('tb_empresas');

			return $id;

		}

	}

	public function update($dadosUpdate = NULL, $id)
	{
		if($dadosUpdate != NULL) {

			$this->db->where('id_empresa', $id)->update('tb_empresas', $dadosUpdate);

			return true;
		
		} else {

			return false;
		}
	}

	public function confirmar_pagamento($id)
	{
		if($id != NULL) {
			$dados['pagamento'] = 'S';

			$registro = $this->db->where('id_empresa', $id)->update('tb_empresas', $dados);

			return $registro;

		} else {

			return false;
		}
	}

	public function cancelar_pagamento($id)
	{
		if($id != NULL) {
			$dados['pagamento'] = 'N';

			$registro = $this->db->where('id_empresa', $id)->update('tb_empresas', $dados);

			return $registro;

		} else {

			return false;
		}
	}


	public function salvarResponsavel($dadosResponsaveis = NULL)
	{
		if($dadosResponsaveis != NULL) {

			$this->db->insert('tb_responsaveis', $dadosResponsaveis);

			return true;

		} else {

			return false;

		}
	}

	public function updateResponsavel($dadosResponsaveis = NULL, $id_responsavel)
	{
		if($dadosResponsaveis != NULL) {

			$this->db
				->where('id_responsavel', $id_responsavel)
				->update('tb_responsaveis', $dadosResponsaveis);

		} else {

			return false;

		}
	}
}