<?php

class Usuarios_Model extends CI_Model
{
	public function findAll()
	{
		$usuarios = $this->db
						->select('tb_usuarios.id_usuario')
						->select('tb_usuarios.login')
						->select('tb_usuarios.nome_usuario')
						->select('tb_grupos_usuario.nome_grupo')
						->select('tb_usuarios.usuario_ativo')
						->join('tb_grupos_usuario', 'tb_grupos_usuario.id_grupo = tb_usuarios.id_grupo')
						->get('tb_usuarios')
						->result();

		return $usuarios;
	}
	

	public function findGrupos()
	{
		$grupos = $this->db->get('tb_grupos_usuario')->result();

		return $grupos;
	}

	public function find($id)
	{
		$usuario = $this->db
					->join('tb_grupos_usuario', 'tb_grupos_usuario.id_grupo = tb_usuarios.id_grupo')
					->where('tb_usuarios.id_usuario', $id)
					->get('tb_usuarios')
					->row();

		return $usuario;
	}

	public function verificaUsuario($login = NULL)
	{
		$verifica = $this->db->where('login', $login)->get('tb_usuarios')->row();

		return $verifica;
	}

	public function insert($dados = NULL)
	{
		if($dados != NULL) {

			$this->db->insert('tb_usuarios', $dados);

			$id = $this->db->insert_id('tb_usuarios');

			return $id;
		}
	}

	public function LogEmail($id = NULL) {

		if($id != NULL) {

			$dados['ultimo_email'] = date('Y-m-d H:i:s');

			$this->db->where('id_usuario', $id)->update('tb_usuarios', $dados);

			return true;
		
		} else {

			return false;
		} 

	}

	public function update($id, $dados)
	{	
		if($dados != NULL) {

			$this->db->where('id_usuario', $id)->update('tb_usuarios', $dados);

			return true;

		} else {

			return false;
		}
	}

	public function autorizaEmail($email)
	{
		$h="";

		if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) { $h="s";} else{$h="";}
		
		$url = "http".$h."://www.inscricaofacil.com.br/webservice_email.php?e=".base64_encode($email);
		
		$info = file_get_contents($url,false);
		
		$obj = json_decode($info);

		if($obj->situacao == 1){
			
			return true; // Liberado
		}
		else{
			
			return false; // Negado
		}
	}
}