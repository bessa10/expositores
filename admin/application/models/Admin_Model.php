<?php

class Admin_Model extends CI_Model
{
	public function get_login($login = NULL, $senha = NULL)
	{

		$usuario = $this->db
					->where('login', $login)
					->where('senha', $senha)
					->where('usuario_ativo', 'S')
					->get('tb_usuarios')
					->row();

		return $usuario;

	}

	public function logAcesso($id = NULL)
	{
		if($id != NULL) {

			$dados['ultimo_acesso'] = date('Y-m-d H:i:s');

			$this->db->where('id_usuario', $id)->update('tb_usuarios', $dados);

			return true;

		} else {

			return false;

		}
	}
}