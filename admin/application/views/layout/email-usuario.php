<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Enviando emails com a library nativa do CodeIgniter</title>
</head>
<body>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr>
            <td class="navbar navbar-inverse">
                <!-- This setup makes the nav background stretch the whole width of the screen. -->
                <table width="650px" cellspacing="0" cellpadding="3" class="container">
                    <tr class="navbar navbar-inverse">
                        <td colspan="4"><h3><?php echo $empresa->nome_evento ?></h3></td><br>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF">
                <table width="650px" cellspacing="0" cellpadding="3" class="container">
                    <tr>
                        <td><p>Prezado(a) <?php echo $empresa->nome_responsavel?>,</p><br></td>
                    </tr>
                    <tr>
                        <td>
                        <p>Seu acesso ao nosso sistema já está liberado, abaixo segue suas informações para o acesso:<br><br>
                        <strong>Login:</strong> <?php echo $empresa->login ?><br><br>
                        <strong>Senha:</strong> <?php echo $empresa->senha ?><br><br>
                        Recomendamos que faça a alteração da senha acessando o perfil do seu usuário.<br><br>
                        Para acessar clique no botão abaixo e preencha seu login e senha. </p><br>
                        </td>
                    </tr>
                    <tr>
                        <td><button><a href="http://www.inscricaofacil.com.br/teste/admin">Entrar no sistema</a></button><br><br></td>
                    </tr>
                    <tr>
                        <td><p>Obs.: Não responda este e-mail, ele é gerado automaticamente<hr></p></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:10px;">
                Para garantir que nossos comunicados cheguem em sua caixa de entrada,
                adicione o email inscricao@inscricaofacil.com.br ao seu catálogo de endereços. <br />
                Abacos respeita a sua privacidade e é contra o spam na rede. <br />
                Se você não deseja mais receber nossos e-mails, cancele  o recebimento <a href="https://www.inscricaofacil.com.br/cancelar_recebimento.php?e=<?php echo $empresa->email_responsavel ?>" target="_blank">aqui</a><br/><br/>
                Estamos localizados em : Rua Gaspar Ricardo Júnior, 104 - Barra Funda - 01136-030 -  Paulo - SP/Brasil<br/><br/>
            </td>
        </tr>
    </table>
</body>
</html>