<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
    <title>Administração de expositores</title>
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?php echo base_url() ?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url() ?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body>
  <div id="wrapper">
    <!-- MENU -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
          <a class="navbar-brand" href="#"><img src="<?php echo base_url() ?>assets/images/logotipo_abacos.png" alt="Ábacos - Soluções em automação" width="150" height="30" /></a>
      </div>
      <!-- PARTE DO LOGOUT -->
      <ul class="nav navbar-top-links navbar-right">
        <span class="label label-primary">
          <?php echo $this->session->userdata('nome_usuario'); ?>
        </span>
        <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <li><a href="<?php echo base_url().'perfil'?>"><i class="fa fa-sign-out fa-fw"></i>&nbsp;Perfil</a></li>
                  <li><a href="<?php echo base_url().'login/logout'?>"><i class="fa fa-sign-out fa-fw"></i>&nbsp;Logout</a></li>
              </ul>
          </li>
      </ul>
      <div class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
          <ul class="nav" id="side-menu">
            <?php if($this->session->userdata('id_grupo') == 1): ?>
              <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;Dashboard</a></li>
              <li>
                <a href="#"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Cadastro<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li><a href="<?php echo base_url() . 'eventos' ?>"><i class="fa fa-calendar"></i>&nbsp;Eventos</a></li>
                  <li><a href="<?php echo base_url() . 'empresas' ?>"><i class="fa fa-suitcase"></i>&nbsp;Empresas</a></li>
                </ul>
              </li>
              <li><a href="<?php echo base_url() . 'empresas/acessos' ?>"><i class="fa fa-list"></i>&nbsp;Relatório de Acessos</a></li>
              <li><a href="<?php echo base_url() . 'financeiro' ?>"><i class="fa fa-money"></i>&nbsp;&nbsp;Financeiro</a></li>
              <li><a href="<?php echo base_url() . 'usuarios' ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Usuários</a></li>
              <li>
                <a href="#"><i class="fa fa-cog"></i>&nbsp;&nbsp;Configuração<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li><a href="<?php echo base_url() . 'eventos' ?>">Criar base</a></li>
                </ul>
              </li>
            <?php elseif($this->session->userdata('id_grupo') == 2): ?>
              <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;Dashboard</a></li>
            <?php elseif($this->session->userdata('id_grupo') == 3): ?>
              <li><a href="<?php echo base_url() . 'financeiro' ?>"><i class="fa fa-money"></i>&nbsp;&nbsp;Financeiro</a></li>
            <?php endif ?>
          </ul>
        </div>
      </div>
    </nav>
    <div id="page-wrapper">
      <?php if($this->session->flashdata()): ?>
        <br>
        <div class="row">
          <div class="col-md-6">
            <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
          </div>
        </div>
      <?php endif ?>
