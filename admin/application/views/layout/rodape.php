            <!-- Core Scripts - Include with every page -->
            <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
            <script src="<?php echo base_url() ?>assets/js/jquery.maskedinput.js"></script>
            <script src="<?php echo base_url() ?>assets/js/geral.js"></script>
            <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
            <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

            <!-- Page-Level Plugin Scripts - Blank -->

            <!-- SB Admin Scripts - Include with every page -->
            <script src="<?php echo base_url() ?>assets/js/sb-admin.js"></script>

            <!-- Page-Level Demo Scripts - Blank - Use for reference -->
            
            <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
            <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/jquery-ui.js"></script>
            <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/jquery.dataTables.columnFilter.js"></script>
            <script src="<?php echo base_url() ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
        </div>
    </div>
</body>
</html>    