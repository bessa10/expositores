<div class="row">
	<div class="col-md-4">
		<h1>Eventos</h1>
	</div>
	<div class="col-md-3 col-md-offset-5">
		<br><a type="button" href="<?php echo base_url() . 'eventos/inserir' ?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Cadastrar novo evento</a>
	</div>
	<div class="row">
		<?php if($this->session->flashdata('message_success')): ?>
			<div class="col-md-6">
				<div class="alert alert-success alert-dismissible" role="alert" id="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<?php echo $this->session->flashdata('message_success'); ?>
				</div>
			</div>
		<?php endif ?>
		<div class="col-md-12"><br>
			<div class="panel panel-default">
			  <div class="panel-heading"><strong>Listagem dos eventos</strong></div>
			  	<div class="panel-body">
				  	<?php if(count($eventos) > 0): ?>
				  	<table class="table table-striped">
				  		<thead>
				  			<tr>
				  				<th>#</th>
				  				<th>Nome do evento</th>
				  				<th class="text-center">Nº centro de custo</th>
				  				<th class="text-center">Início</th>
				  				<th class="text-center">Término</th>
				  				<th class="text-center">Organizador</th>
				  				<th class="text-center">Editar</th>
				  			</tr>
				  		</thead>
				  		<tbody>
				  			<?php foreach($eventos as $evento): ?>
				  			<tr>
				  				<td><?php echo $evento->id_evento ?></td>
				  				<td><?php echo $evento->nome_evento ?></td>
				  				<td class="text-center"><?php echo $evento->num_centro_custo ?></td>
				  				<td class="text-center"><?php echo $evento->data_inicio ?></td>
				  				<td class="text-center"><?php echo $evento->data_termino ?></td>
				  				<td class="text-center"><?php echo $evento->organizador ?></td>
				  				<td class="text-center"><a href="<?php echo base_url() . 'eventos/editar/' . $evento->id_evento ?>" class="btn btn-success"><i class="fa fa-edit"></i></a></td>
				  			</tr>
				  			<?php endforeach ?>
				  		</tbody>
				  	</table>
				  <?php else: ?>	
				  	<h4>Nenhum evento encontrado...</h4>
				  <?php endif ?>	
			  	</div>
			</div>
		</div>
	</div>
</div>