<div class="row">
	<div class="row">
		<div class="col-md-4">
			<h1>Editar evento</h1>
		</div>
		<div class="col-md-4 col-md-offset-3">
			<br><a href="<?php echo base_url() . 'eventos' ?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
		</div>
	</div>
	<?php echo form_open('eventos/editar/' . $evento->id_evento); ?>	
		<input type="hidden" name="id_evento" value="<?php echo $evento->id_evento ?>">
		<div class="row">
			<div class="form-group col-md-2 <?php echo (form_error('num_centro_custo'))?'has-error':''?>">
				<strong>Nº centro de custo</strong>
				<input type="text" class="form-control" value="<?php echo $evento->num_centro_custo ?>" name="num_centro_custo" aria-describedby="helpBlock2">
				<span id="helpBlock2" class="help-block"><?php echo form_error('num_centro_custo') ?></span>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6 <?php echo (form_error('nome_evento'))?'has-error':''?>">
				<strong>Nome do evento</strong>
				<input type="text" class="form-control" value="<?php echo $evento->nome_evento ?>" name="nome_evento" aria-describedby="helpBlock2">
				<span id="helpBlock2" class="help-block"><?php echo form_error('nome_evento') ?></span>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-3 <?php echo (form_error('data_inicio'))?'has-error':''?>">
				<strong>Data de início</strong>
				<input type="date" class="form-control" value="<?php echo $evento->data_inicio ?>" name="data_inicio" aria-describedby="helpBlock2">
				<span id="helpBlock2" class="help-block"><?php echo form_error('data_inicio') ?></span>
			</div>
			<div class="form-group col-md-3 <?php echo (form_error('data_termino'))?'has-error':''?>">
				<strong>Data do término</strong>
				<input type="date" class="form-control" value="<?php echo $evento->data_termino ?>" name="data_termino" aria-describedby="helpBlock2">
				<span id="helpBlock2" class="help-block"><?php echo form_error('data_termino') ?></span>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6 <?php echo (form_error('organizador'))?'has-error':''?>">
				<strong>Organizador</strong>
				<input type="text" class="form-control" value="<?php echo $evento->organizador ?>" name="organizador" aria-describedby="helpBlock2">
				<span id="helpBlock2" class="help-block"><?php echo form_error('organizador') ?></span>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-2">
				<button type="submit" class="btn btn-primary">Salvar evento</button>
			</div>
		</div>
	</form>
</div>
