<div class="row">
	<div class="col-md-4">
		<h1>Novo evento</h1>
	</div>
	<div class="col-md-2 col-md-offset-6">
		<br><a href="<?php echo base_url().'eventos'?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
	</div>
	<div class="col-md-12">
		<?php echo form_open('eventos/inserir'); ?>
			<div class="panel panel-default">
			<div class="panel-heading"><strong>Dados do evento</strong></div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-2 <?php echo (form_error('num_centro_custo')) ? 'has-error' : '' ?>">
							<strong>Nº centro de custo</strong>
							<input type="text" class="form-control" name="num_centro_custo" value='<?php set_value('num_centro_custo') ?>' aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('num_centro_custo') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6 <?php echo (form_error('nome_evento')) ? 'has-error' : '' ?>">
							<strong>Nome do evento</strong>
							<input type="text" class="form-control" name="nome_evento" value="<?php set_value('nome_evento') ?>" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('nome_evento') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-3 <?php echo (form_error('data_inicio')) ? 'has-error' : '' ?>">
							<strong>Data de início</strong>
							<input type="date" class="form-control" name="data_inicio" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('data_inicio') ?></span>
						</div>
						<div class="form-group col-md-3 <?php echo (form_error('data_termino')) ? 'has-error' : '' ?>">
							<strong>Data do término</strong>
							<input type="date" class="form-control" name="data_termino" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('data_termino') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6 <?php echo (form_error('organizador')) ? 'has-error' : '' ?>">
							<strong>Organizador</strong>
							<input type="text" class="form-control" name="organizador" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('organizador') ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2">
					<button type="submit" class="btn btn-primary">Cadastrar evento</button>
				</div>
			</div>
		</form>
	</div>
</div>
