<!DOCTYPE html>
<html>
<meta charset="utf-8">
<head>
    <title>Administração de expositores</title>
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url() ?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body style="background-image:url(/admin/assets/images/fundo.jpg)">
	<div class="container">
		<div class="col-md-4 col-md-offset-4"><br><br>
			<div class="panel panel-primary">
				<div class="panel-heading text-center"><strong>Login administração expositores</strong></div>
				<div class="panel-body">
					<?php if($this->session->flashdata()): ?>
				        <br>
				        <div class="row">
				          <div class="col-md-12">
				            <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissible" role="alert" id="alert">
				                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                  <span aria-hidden="true">&times;</span>
				                </button>
				                <?php echo $this->session->flashdata('msg'); ?>
				            </div>
				          </div>
				        </div>
				    <?php endif ?>
					<form class="" action="<?php echo base_url() . 'login' ?>" method="POST">
						<div class="form-group input-group <?php echo (form_error('login')) ? 'has-error' : '' ?>">
	  						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
	  						<input type="text" class="form-control" name="login" placeholder="Login" aria-describedby="helpBlock2">
						</div>
						<div class="form-group has-error">
							<span id="helpBlock2" class="help-block"><?php echo form_error('login') ?></span>
						</div>
						<div class="form-group input-group <?php echo (form_error('senha')) ? 'has-error' : '' ?>">
	  						<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
	  						<input type="password" class="form-control" name="senha" placeholder="Senha" aria-describedby="helpBlock2">
						</div>
						<div class="form-group has-error">
							<span id="helpBlock2" class="help-block"><?php echo form_error('senha') ?></span>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block">Login</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url() ?>assets/js/sb-admin.js"></script>
</body>
</html>
