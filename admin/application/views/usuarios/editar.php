<div class="row">
	<div class="col-md-10">
		<h1>Editar - <?php echo $usuario->nome_usuario ?></h1>
	</div>
	<div class="col-md-2">
		<br><a href="<?php echo base_url().'usuarios'?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
	</div>
	<div class="col-md-12">
		<?php echo form_open('usuarios/editar/' . $usuario->id_usuario); ?>	
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Dados do usuário</strong></div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-6 <?php echo (form_error('nome_usuario'))?'has-error':''?>">
							<strong>Nome</strong>
							<input type="text" class="form-control" value="<?php echo $usuario->nome_usuario ?>" name="nome_usuario" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('nome_usuario') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
							<strong>Login</strong>
							<input type="text" class="form-control" name="login" value="<?php echo $usuario->login ?>" disabled>
						</div>
						<?php 
							if(form_error('senha') or form_error('confirmar_senha')) {
								$display2 = 'none';
							} else {
								$display2 = 'block';							
							}
						?>
						<div class="form-group col-md-4" style="display: <?= $display2 ?>;">
							<br><button type="button" class="btn btn-sm btn-primary" onclick="AddSenha()" id="bt-senha"><i class="fa fa-lock"></i>&nbsp;&nbsp;Alterar senha</button>
						</div>
					</div>
					<?php $display = (form_error('senha')) ? 'block' : 'none'; ?>
					<div class="senha" style="display: <?= $display ?>;">
						<div class="row">
							<div class="form-group col-md-3 <?php echo (form_error('senha'))?'has-error':''?>">
								<strong>Nova senha</strong>
								<input type="password" class="form-control" name="senha" aria-describedby="helpBlock2">
								<span id="helpBlock2" class="help-block"><?php echo form_error('senha') ?></span>
							</div>
							<div class="form-group col-md-3 <?php echo (form_error('confirmar_senha'))?'has-error':''?>">
								<strong>Confirmar nova senha</strong>
								<input type="password" class="form-control" name="confirmar_senha" aria-describedby="helpBlock2">
								<span id="helpBlock2" class="help-block"><?php echo form_error('confirmar_senha') ?></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4">
							<strong>Grupo de usuário</strong>
							<select class="form-control" name="id_grupo">
								<option value="<?php echo $usuario->id_grupo ?>"><?php echo $usuario->nome_grupo ?></option>
								<?php foreach($grupos as $grupo): ?>
									<?php if($grupo->id_grupo != $usuario->id_grupo): ?>
										<option value="<?php echo $grupo->id_grupo ?>"><?php echo $grupo->nome_grupo ?></option>
									<?php endif ?>
								<?php endforeach ?>
							</select>
						</div>
						<div class="form-group col-md-4">
							<strong>Status</strong>
							<?php $status = ($this->session->userdata('id_usuario') == $usuario->id_usuario) ? 'disabled' : ''; ?>
							<select class="form-control" name="usuario_ativo" <?= $status ?>>
								<?php if($usuario->usuario_ativo == 'S'): ?>
									<option value="S">Ativo</option>
									<option value="N">Inativo</option>
								<?php else: ?>
									<option value="N">Inativo</option>
									<option value="S">Ativo</option>
								<?php endif ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2">
					<button type="submit" class="btn btn-primary">Salvar alterações</button>
				</div>
			</div>
		</form>
	</div>
</div>