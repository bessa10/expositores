<div class="row">
	<div class="col-md-4">
		<h1>Novo usuário</h1>
	</div>
	<div class="col-md-2 col-md-offset-6">
		<br><a href="<?php echo base_url().'usuarios'?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
	</div>
	<div class="col-md-12">
			<?php echo form_open('usuarios/inserir'); ?>
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Dados do usuário</strong></div>
				<div class="panel-body">
					<div class="row">
						<div class="form-group col-md-6 <?php echo (form_error('nome_usuario')) ? 'has-error' : '' ?>">
							<strong>Nome</strong>
							<input type="text" class="form-control" value="<?php echo set_value('nome_usuario') ?>" name="nome_usuario" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('nome_usuario') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4 <?php echo (form_error('login')) ? 'has-error' : '' ?>">
							<strong>Login</strong>
							<input type="text" class="form-control" value="<?php echo set_value('login') ?>" name="login" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('login') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-3 <?php echo (form_error('senha')) ? 'has-error' : '' ?>">
							<strong>Senha</strong>
							<input type="password" class="form-control" name="senha" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('senha') ?></span>
						</div>
						<div class="form-group col-md-3 <?php echo (form_error('confirmar_senha')) ? 'has-error' : '' ?>">
							<strong>Confirmar senha</strong>
							<input type="password" class="form-control" name="confirmar_senha" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('confirmar_senha') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-4 <?php echo (form_error('id_grupo')) ? 'has-error' : '' ?>">
							<strong>Grupo de usuário</strong>
							<select class="form-control" name="id_grupo" aria-describedby="helpBlock2">
								<option value="">Selecione</option>
								<?php foreach($grupos as $grupo): ?>
									<option value="<?php echo $grupo->id_grupo ?>"><?php echo $grupo->nome_grupo ?></option>
								<?php endforeach ?>
							</select>
							<span id="helpBlock2" class="help-block"><?php echo form_error('id_grupo') ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2">
					<button type="submit" class="btn btn-primary">Cadastrar usuário</button>
				</div>
			</div>
		</form>
	</div>
</div>
