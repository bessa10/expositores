<div class="row">
	<div class="row">
		<div class="col-md-4">
			<h1>Usuários</h1>
		</div>
		<div class="col-md-3 col-md-offset-5">
			<br><a type="button" href="<?php echo base_url() . 'usuarios/inserir' ?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Cadastrar novo usuário</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12"><br>
			<div class="panel panel-default">
			  <div class="panel-heading"><strong>Listagem dos usuários</strong></div>
			  	<div class="panel-body">
				  	<table class="table table-striped">
				  		<thead>
				  			<tr>
				  				<th>Nome</th>
				  				<th class="text-center">Login</th>
				  				<th class="text-center">Grupo</th>
				  				<th class="text-center">Status</th>
				  				<th class="text-center">Editar</th>
				  			</tr>
				  		</thead>
				  		<tbody>
				  			<?php foreach($usuarios as $usuario): ?>
				  			<tr>
				  				<td><?php echo $usuario->nome_usuario ?></td>
				  				<td class="text-center"><?php echo $usuario->login ?></td>
				  				<td class="text-center"><?php echo $usuario->nome_grupo ?></td>
				  				<?php if($usuario->usuario_ativo == 'S'): ?>
				  					<td class="text-center"><span style="color:#5cb85c">Ativo</span></td>
				  				<?php else: ?>
				  					<td class="text-center"><span style="color:#d9534f">Inativo</span></td>
				  				<?php endif ?>
				  				<td class="text-center"><a href="<?php echo base_url().'usuarios/editar/'.$usuario->id_usuario?>" class="btn btn-success"><i class="fa fa-edit"></i></a></td>
				  				<!--<td class="text-center"><button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm" onclick="setaDadoModal(<?= $usuario->id_usuario ?>)"><i class="fa fa-times"></i></button></td>-->
				  			</tr>
				  			<?php endforeach ?>
				  		</tbody>
				  	</table>
			  	</div>
			</div>
		</div>
	</div>
	<!-- Modal
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	    	<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><br>
	    	</div>
	    	<div class="modal-body">
        		<p>Deseja realmente desativar o usuário?</p>
      		</div>
      		<div class="modal-footer">
        		<form action="<?php echo base_url() . 'usuarios/desativar' ?>" method="POST">
        			<input type="hidden" id="id_usuario" name="id_usuario" value="">
        			<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        			<button type="submit" class="btn btn-success" id="bt-editar">Sim</button>
        		</form>
      		</div>
	    </div>
	  </div>
	</div> -->
</div>