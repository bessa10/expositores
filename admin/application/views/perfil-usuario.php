<div class="row">
	<div class="col-md-10">
		<h1>Perfil - <?php echo $usuario->nome_usuario ?></h1>
	</div>
	<div class="col-md-2">
		<br><a href="<?php echo base_url()?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
	</div>
	<div class="col-md-12">
		<?php echo form_open('perfil'); ?>
			<div class="panel panel-default">
				<div class="panel-heading">Dados do usuário</div>
				<div class="panel-body">
					<div class="row">
						<input type="hidden" name="id_usuario" value="<?php echo $usuario->id_usuario ?>">
						<div class="form-group col-md-4 <?php echo (form_error('login'))?'has-error':''?>">
							<strong>Login</strong>
							<input type="text" class="form-control" name="login" value="<?php echo $usuario->login ?>" disabled>
							<span id="helpBlock2" class="help-block"><?php echo form_error('login') ?></span>
						</div>
						<?php 
							if(form_error('senha') or form_error('confirmar_senha')) {
								$style1 = 'none';
							} else {
								$style1 = 'block';							
							}
						?>
						<div class="form-group col-md-4" style="display: <?php echo $style1 ?>;">
							<br><button type="button" id="bt-senha" onclick="AddSenha()" class="btn btn-primary btn-sm"><i class="fa fa-lock"></i>&nbsp;&nbsp;Alterar senha</button>
						</div>
					</div>
					<?php $style = (form_error('confirmar_senha') ? 'block' : 'none');?>
					<div class="senha" style="display: <?php echo $style ?>;">
						<div class='row'>
							<div class="form-group col-md-3 <?php echo (form_error('senha'))?'has-error':''?>">
								<strong>Nova senha</strong>
								<input type='password' class='form-control' name='senha'>
								<span id="helpBlock2" class="help-block"><?php echo form_error('senha') ?></span>
							</div>
							<div class="form-group col-md-3 <?php echo (form_error('confirmar_senha'))?'has-error':''?>">
								<strong>Confirmar nova senha</strong>
								<input type='password' class='form-control' name='confirmar_senha'>
								<span id="helpBlock2" class="help-block"><?php echo form_error('confirmar_senha') ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-2">
					<button type="submit" class="btn btn-primary">Salvar alterações</button>
				</div>
			</div>
		</form>
	</div>
</div>