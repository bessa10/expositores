<div class="row">
	<div class="col-md-12">
		<div class="row">
			<h1>Financeiro</h1><hr>
			<form class="" action="<?php echo base_url() . 'financeiro' ?>" method="POST">
				<div class="form-group col-md-4">
					<input type="text" class="form-control" name="nome_empresa" value="<?php echo $this->session->flashdata('nome_empresa') ?>" placeholder="Nome da empresa:">
				</div>
				<div class="form-group col-md-3">
					<input type="text" class="form-control" name="num_contrato" value="<?php echo $this->session->flashdata('num_contrato') ?>" placeholder="Nº do contrato:">
				</div>
				<div class="form-group col-md-3">
					<input type="text" class="form-control" name="cnpj_empresa" id="cnpj_empresa" value="<?php echo $this->session->flashdata('cnpj_empresa') ?>" placeholder="CNPJ:">
				</div>
				<div class="form-group col-md-1">
					<button type="submit" class="btn btn-primary">Filtrar</button>
				</div>
			</form>
		</div><hr>
		<div class="row">
			<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Listagem das empresas cadastradas</strong></div>
				<div class="panel-body">
					<?php if(count($empresas) > 0): ?>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Nome da empresa</th>
								<th class="text-center">Nº do contrato</th>
								<th class="text-center">CNPJ</th>
								<th class="text-center">Status</th>
								<th class="text-center">Baixar pagamento</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($empresas as $empresa): ?>
							<tr>
								<td><?php echo $empresa->nome_empresa ?></td>
								<td class="text-center"><?php echo $empresa->num_contrato ?></td>
								<td class="text-center"><?php echo $empresa->cnpj_empresa ?></td>
								<?php if($empresa->pagamento == 'S'): ?>
									<td class="text-center"><span class="label label-success">Pago</span></td>
								<?php else: ?>
									<td class="text-center"><span class="label label-warning">Aguardando</span></td>
								<?php endif ?>
								<?php if($empresa->pagamento == 'S'): ?>
									<td class="text-center"><a href="<?php echo base_url() . 'financeiro/cancelar_pagamento/' . $empresa->id_empresa?>" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Cancelar</a></td>
								<?php else: ?>
									<td class="text-center"><a href="<?php echo base_url() . 'financeiro/confirmar_pagamento/' . $empresa->id_empresa?>" class="btn btn-success btn-sm"><i class="fa fa-check"></i>&nbsp;Confirmar</a></td>
								<?php endif ?>	
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
					<?php else: ?>
						<h4>Nenhuma empresa cadastrada...</h4>
					<?php endif ?>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
