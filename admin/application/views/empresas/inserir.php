<div class="row">
	<div class="col-md-4">
		<h1>Nova empresa</h1>
	</div>
	<div class="col-md-2 col-md-offset-6">
		<br><a href="<?php echo base_url() . 'empresas' ?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
	</div>
	<div class="col-md-12">
	<?php echo form_open('empresas/inserir'); ?>	
		<div class="panel panel-default">
			<div class="panel-heading"><strong>Dados da empresa</strong></div>
			<div class="panel-body">
				<div class="row">
					<div class="form-group col-md-8 <?php echo (form_error('nome_empresa')) ? 'has-error' : '' ?>">
						<strong>Nome da empresa</strong>
						<input type="text" class="form-control" value="<?php echo set_value('nome_empresa')?>" name="nome_empresa" aria-describedby="helpBlock2">
						<span id="helpBlock2" class="help-block"><?php echo form_error('nome_empresa') ?></span>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-4 <?php echo (form_error('num_contrato')) ? 'has-error' : '' ?>">
						<strong>Nº do contrato</strong>
						<input type="text" class="form-control" value="<?php echo set_value('num_contrato')?>" name="num_contrato" aria-describedby="helpBlock2">
						<span id="helpBlock2" class="help-block"><?php echo form_error('num_contrato') ?></span>
					</div>
					<div class="form-group col-md-4 <?php echo (form_error('cnpj_empresa')) ? 'has-error' : '' ?>">
						<strong>CNPJ</strong>
						<input type="text" class="form-control" value="<?php echo set_value('cnpj_empresa')?>" name="cnpj_empresa" id="cnpj_empresa" aria-describedby="helpBlock2">
						<span id="helpBlock2" class="help-block"><?php echo form_error('cnpj_empresa') ?></span>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-8  <?php echo (form_error('id_evento')) ? 'has-error' : '' ?>">
						<strong>Evento</strong>
						<select class="form-control" name="id_evento">
							<option value="">Selecione</option>
							<?php foreach($eventos as $evento): ?>	
								<option value="<?php echo $evento->id_evento ?>"><?php echo $evento->nome_evento ?></option>
							<?php endforeach ?>
						</select>
						<span id="helpBlock2" class="help-block"><?php echo form_error('id_evento') ?></span>
					</div>
				</div>
				<div id="responsavel">
					<div class="row">
						<div class="form-group col-md-8 <?php echo (form_error('nome_responsavel[]')) ? 'has-error' : '' ?>">
							<strong>Nome do responsável</strong>
							<input type="text" class="form-control" value="<?php echo set_value('nome_responsavel[]')?>" name="nome_responsavel[]" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('nome_responsavel[]') ?></span>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-8 <?php echo (form_error('email_responsavel[]')) ? 'has-error' : '' ?>">
							<strong>E-mail responsável</strong>
							<input type="email" class="form-control" value="<?php echo set_value('email_responsavel[]')?>" name="email_responsavel[]" aria-describedby="helpBlock2">
							<span id="helpBlock2" class="help-block"><?php echo form_error('email_responsavel[]') ?></span>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<button type="button" id="btnAdd" onclick="AddElemento()" class="btn btn-success btn-sm">+ Responsáveis</button>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<button type="submit" class="btn btn-primary">Cadastrar empresa</button>
			</div>
		</div>
	</form>
	</div>
</div>
