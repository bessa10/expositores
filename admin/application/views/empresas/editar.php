<div class="row">
		<div class="col-md-10">
			<h1>Editar - <?php echo $empresa->nome_empresa ?></h1>
		</div>
		<div class="col-md-2">
			<br><a href="<?php echo base_url().'empresas'?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Voltar</a>
		</div>
	<div class="row">
	<div class="col-md-12">
		<?php echo form_open('empresas/editar/' . $empresa->id_empresa) ?>	
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><strong>Dados da empresa</strong></div>
					<div class="panel-body">
						<div class="row">
							<div class="form-group col-md-3 <?php echo (form_error('num_contrato')) ? 'has-error' : '' ?>">
								<strong>Nº do contrato</strong>
								<input type="text" class="form-control" value="<?php echo $empresa->num_contrato ?>" name="num_contrato" aria-describedby="helpBlock2">
								<span id="helpBlock2" class="help-block"><?php echo form_error('num_contrato') ?></span>
							</div>
							<div class="form-group col-md-4 <?php echo (form_error('cnpj_empresa')) ? 'has-error' : '' ?>">
								<strong>CNPJ</strong>
								<input type="text" class="form-control" value="<?php echo $empresa->cnpj_empresa ?>" name="cnpj_empresa" id="cnpj_empresa" aria-describedby="helpBlock2">
								<span id="helpBlock2" class="help-block"><?php echo form_error('cnpj_empresa') ?></span>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8 <?php echo (form_error('nome_empresa')) ? 'has-error' : '' ?>">
								<strong>Nome</strong>
								<input type="text" class="form-control" value="<?php echo $empresa->nome_empresa ?>" name="nome_empresa" aria-describedby="helpBlock2">
								<span id="helpBlock2" class="help-block"><?php echo form_error('nome_empresa') ?></span>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8">
								<strong>Evento</strong>
								<select class="form-control" name="id_evento">
									<option value="<?php echo $empresa->id_evento ?>"><?php echo $empresa->nome_evento ?></option>
									<?php foreach($eventos as $evento): ?>
										<?php if($evento->id_evento != $empresa->id_evento): ?>
										<option value="<?php echo $evento->id_evento ?>"><?php echo $evento->nome_evento ?></option>
										<?php endif ?>
									<?php endforeach ?>
								</select>
							</div>
						</div>
						<div id="responsavel">
							<?php foreach($responsaveis as $responsavel): ?>
							<input type='hidden' name='id_responsavel[]' value='<?php echo $responsavel->id_responsavel?>'><hr>	
							<div class="row">
								<div class="form-group col-md-8 <?php echo (form_error('nome_responsavel[]')) ? 'has-error' : '' ?>">
									<strong>Nome do responsável</strong>
									<input type="text" class="form-control" value="<?php echo $responsavel->nome_responsavel ?>" name="nome_responsavel[]" aria-describedby="helpBlock2">
									<span id="helpBlock2" class="help-block"><?php echo form_error('nome_responsavel[]') ?></span>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-8 <?php echo (form_error('email_responsavel[]')) ? 'has-error' : '' ?>">
									<strong>E-mail responsável</strong>
									<input type="email" class="form-control" value="<?php echo $responsavel->email_responsavel ?>" name="email_responsavel[]" aria-describedby="helpBlock2">
									<span id="helpBlock2" class="help-block"><?php echo form_error('email_responsavel[]') ?></span>
								</div>
							</div>
							<?php endforeach ?>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<button type="button" id="btnAdd" onclick="AddElemento()" class="btn btn-success btn-sm">+ Responsáveis</button>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="form-group col-md-4">
				<button type="submit" class="btn btn-primary">Salvar empresa</button>
			</div>
		</form>
	</div>
	</div>
</div>