<div class="row">
	<div class="col-md-4">
		<h1>Empresas</h1>
	</div>
	<div class="col-md-2 col-md-offset-5">
		<br><a href="<?php echo base_url() . 'empresas/inserir'?>" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Cadastrar nova empresa</a>
	</div>
	<div class="row">
		<?php if($this->session->flashdata('message_success')): ?>
			<div class="col-md-6">
				<div class="alert alert-success alert-dismissible" role="alert" id="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<?php echo $this->session->flashdata('message_success'); ?>
				</div>
			</div>
		<?php endif ?>
		<div class="col-md-12"><br>
		  	<div class="panel panel-default">
		  		<div class="panel-heading"><strong>Listagem das empresas cadastradas</strong></div>
		  		<div class="panel-body">
		  		<?php if(count($empresas) > 0): ?>
		  		<table class="table table-striped">
			  		<thead>
			  			<tr>
			  				<th>Nome da empresa</th>
			  				<th class="text-center">Nº do contrato</th>
			  				<th class="text-center">CNPJ</th>
			  				<th class="text-center">Evento</th>
			  				<th class="text-center">Editar</th>
			  			</tr>
			  		</thead>
			  		<tbody>
			  			<?php foreach($empresas as $empresa): ?>
			  			<tr>
			  				<td><?php echo $empresa->nome_empresa ?></td>
			  				<td class="text-center"><?php echo $empresa->num_contrato ?></td>
			  				<td class="text-center"><?php echo $empresa->cnpj_empresa ?></td>
			  				<td class="text-center"><?php echo $empresa->nome_evento ?></td>
			  				<td class="text-center"><a href="<?php echo base_url() . 'empresas/editar/' . $empresa->id_empresa ?>" class="btn btn-success"><i class="fa fa-edit"></i></a></td>
			  			</tr>
			  			<?php endforeach ?>
			  		</tbody>
		  		</table>
		  		<?php else: ?>
		  			<h4>Nenhuma empresa cadastrada...</h4>
		  		<?php endif ?>
		  	</div>
		  	</div>
		</div>
	</div>	
</div>
