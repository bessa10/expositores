<div class="row">
	<div class="col-md-12">
		<h1>Relatório de acessos</h1><hr>
		<div class="panel panel-default">
			<div class="panel-heading"><strong>Lista dos usuários das empresas</strong></div>
			<div class="panel-body">
				<form action="<?php echo base_url().'usuarios/enviar_emails' ?>" method="POST" name="form1">
				<?php if(count($empresas) > 0): ?>
				<table class="table table-striped table-responsive">
					<thead>
						<tr>
							<th>Empresa</th>
							<th class="text-center">Responsável</th>
							<th class="text-center">Acesso</th>
							<th class="text-center">Último e-mail</th>
							<th class="text-center"><input type="checkbox" name="marcar" onclick="CheckAll()"><span id="acao"> Todos</span></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($empresas as $empresa): ?>
							<tr>
								<td><?php echo $empresa->nome_empresa ?></td>
								<td class="text-center"><?php echo $empresa->nome_responsavel ?></td>
								<td class="text-center"><?php echo ($empresa->ultimo_acesso) ? $empresa->ultimo_acesso : "<span class='label label-warning'>Aguardando</span>" ?></td>
								<td class="text-center"><?php echo ($empresa->ultimo_email) ? $empresa->ultimo_email : '-' ?></td>
								<td class="text-center"><input type="checkbox" class="" id="btn-checkbox" name="email[]" value="<?php echo $empresa->id_responsavel ?>"> Marcar</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<?php else: ?>
					<h4>Nenhum registro encontrado...</h4>
				<?php endif ?>
			</div>
		</div>
		<div class="col-md-1">
			<button type="submit" class="btn btn-success">Enviar e-mails selecionados</button>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	
</script>